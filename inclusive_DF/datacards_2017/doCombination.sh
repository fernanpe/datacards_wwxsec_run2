#!/bin/bash

combineCards.py   ww2l2v_13TeV_ww_sr_eu_0j=ww2l2v_13TeV_sr_0j_B0/BDTOutput_0j/datacard.txt  \
				  ww2l2v_13TeV_ww_sr_eu_1j=ww2l2v_13TeV_sr_1j_B0/BDTOutput_1j/datacard.txt  \
				  ww2l2v_13TeV_ww_sr_eu_2j=ww2l2v_13TeV_sr_2j_B0/BDTOutput_2j/datacard.txt  \
				  ww2l2v_13TeV_top_0j=ww2l2v_13TeV_top_0j/events/datacard.txt \
				  ww2l2v_13TeV_top_1j=ww2l2v_13TeV_top_1j/events/datacard.txt \
				  ww2l2v_13TeV_top_2j=ww2l2v_13TeV_top_2j/events/datacard.txt \
				  > datacard_combined.txt

echo "nuisance edit drop Vg ww2l2v_13TeV_top_0j CMS_scale_met_2017" >> datacard_combined.txt

text2workspace.py datacard_combined.txt -o datacard_combined.root -m 125

combine -M FitDiagnostics --rMin=-10 --rMax=10 datacard_combined.txt -t -1 --expectSignal=1 -m 125 &> datacard_combined.out
