#!/bin/bash

combineCards.py ../datacards_2016_HIPM/ww2l2v_13TeV_sr_0j_B0/BDTOutput_0j/datacard.txt  \
    			../datacards_2016_HIPM/ww2l2v_13TeV_sr_1j_B0/BDTOutput_1j/datacard.txt  \
    			../datacards_2016_HIPM/ww2l2v_13TeV_sr_2j_B0/BDTOutput_2j/datacard.txt  \
    			../datacards_2016_HIPM/ww2l2v_13TeV_top_0j/events/datacard.txt \
    			../datacards_2016_HIPM/ww2l2v_13TeV_top_1j/events/datacard.txt \
    			../datacards_2016_HIPM/ww2l2v_13TeV_top_2j/events/datacard.txt \
    			../datacards_2016_noHIPM/ww2l2v_13TeV_sr_0j_B0/BDTOutput_0j/datacard.txt  \
    			../datacards_2016_noHIPM/ww2l2v_13TeV_sr_1j_B0/BDTOutput_1j/datacard.txt  \
    			../datacards_2016_noHIPM/ww2l2v_13TeV_sr_2j_B0/BDTOutput_2j/datacard.txt  \
    			../datacards_2016_noHIPM/ww2l2v_13TeV_top_0j/events/datacard.txt \
    			../datacards_2016_noHIPM/ww2l2v_13TeV_top_1j/events/datacard.txt \
    			../datacards_2016_noHIPM/ww2l2v_13TeV_top_2j/events/datacard.txt \
				../datacards_2017/ww2l2v_13TeV_sr_0j_B0/BDTOutput_0j/datacard.txt  \
			    ../datacards_2017/ww2l2v_13TeV_sr_1j_B0/BDTOutput_1j/datacard.txt  \
			    ../datacards_2017/ww2l2v_13TeV_sr_2j_B0/BDTOutput_2j/datacard.txt  \
			    ../datacards_2017/ww2l2v_13TeV_top_0j/events/datacard.txt \
			    ../datacards_2017/ww2l2v_13TeV_top_1j/events/datacard.txt \
			    ../datacards_2017/ww2l2v_13TeV_top_2j/events/datacard.txt \
			    ../datacards_2018/ww2l2v_13TeV_sr_0j_B0/BDTOutput_0j/datacard.txt  \
			    ../datacards_2018/ww2l2v_13TeV_sr_1j_B0/BDTOutput_1j/datacard.txt  \
			    ../datacards_2018/ww2l2v_13TeV_sr_2j_B0/BDTOutput_2j/datacard.txt  \
			    ../datacards_2018/ww2l2v_13TeV_top_0j/events/datacard.txt \
			    ../datacards_2018/ww2l2v_13TeV_top_1j/events/datacard.txt \
			    ../datacards_2018/ww2l2v_13TeV_top_2j/events/datacard.txt \
			    > datacard_combined.txt

echo "nuisance edit drop Vg ch16 CMS_scale_met_2017" >> datacard_combined.txt

text2workspace.py datacard_combined.txt -o datacard_combined.root -m 125

combine -M FitDiagnostics --rMin=-10 --rMax=10 datacard_combined.txt -t -1 --expectSignal=1 -m 125 &> datacard_combined.out
