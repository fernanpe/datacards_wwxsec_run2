#!/bin/bash

## FIXME this is where the Combine framework is installed
#cd ~/Combine/CMSSW_10_2_13/src/
#eval `scramv1 runtime -sh`
#cd -

ulimit -s unlimited

combineCards.py   ww2l2v_13TeV_ww_sr_eu_0j_B0=ww2l2v_13TeV_sr_0j_B0/BDTOutput_0j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_1j_B0=ww2l2v_13TeV_sr_1j_B0/BDTOutput_1j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_2j_B0=ww2l2v_13TeV_sr_2j_B0/BDTOutput_2j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_0j_B1=ww2l2v_13TeV_sr_0j_B1/BDTOutput_0j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_1j_B1=ww2l2v_13TeV_sr_1j_B1/BDTOutput_1j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_2j_B1=ww2l2v_13TeV_sr_2j_B1/BDTOutput_2j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_0j_B2=ww2l2v_13TeV_sr_0j_B2/BDTOutput_0j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_1j_B2=ww2l2v_13TeV_sr_1j_B2/BDTOutput_1j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_2j_B2=ww2l2v_13TeV_sr_2j_B2/BDTOutput_2j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_0j_B3=ww2l2v_13TeV_sr_0j_B3/BDTOutput_0j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_1j_B3=ww2l2v_13TeV_sr_1j_B3/BDTOutput_1j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_2j_B3=ww2l2v_13TeV_sr_2j_B3/BDTOutput_2j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_0j_B4=ww2l2v_13TeV_sr_0j_B4/BDTOutput_0j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_1j_B4=ww2l2v_13TeV_sr_1j_B4/BDTOutput_1j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_2j_B4=ww2l2v_13TeV_sr_2j_B4/BDTOutput_2j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_0j_B5=ww2l2v_13TeV_sr_0j_B5/BDTOutput_0j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_1j_B5=ww2l2v_13TeV_sr_1j_B5/BDTOutput_1j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_2j_B5=ww2l2v_13TeV_sr_2j_B5/BDTOutput_2j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_0j_B6=ww2l2v_13TeV_sr_0j_B6/BDTOutput_0j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_1j_B6=ww2l2v_13TeV_sr_1j_B6/BDTOutput_1j/datacard.txt  \
                  ww2l2v_13TeV_ww_sr_eu_2j_B6=ww2l2v_13TeV_sr_2j_B6/BDTOutput_2j/datacard.txt  \
                  ww2l2v_13TeV_top_0j=ww2l2v_13TeV_top_0j/events/datacard.txt \
                  ww2l2v_13TeV_top_1j=ww2l2v_13TeV_top_1j/events/datacard.txt \
                  ww2l2v_13TeV_top_2j=ww2l2v_13TeV_top_2j/events/datacard.txt \
                  > datacard_combined.txt

text2workspace.py -P HiggsAnalysis.CombinedLimit.PhysicsModel:multiSignalModel \
                  --PO 'map=.*/.*WW_B0:r_0[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B1:r_1[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B2:r_2[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B3:r_3[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B4:r_4[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B5:r_5[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B6:r_6[1, -10, 10]' \
                  datacard_combined.txt -o datacard_combined.root

combine -M MultiDimFit --algo singles -t -1 --setParameters r_0=1,r_1=1,r_2=1,r_3=1,r_4=1,r_5=1,r_6=1 -d datacard_combined.root --X-rtd MINIMIZER_analytic > datacard_combined.out
