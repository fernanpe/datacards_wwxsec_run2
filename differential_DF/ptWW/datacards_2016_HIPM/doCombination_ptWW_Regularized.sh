#!/bin/bash

ulimit -s unlimited

#combineCards.py   ww2l2v_13TeV_ww_sr_eu_0j_B0=ww2l2v_13TeV_sr_0j_B0/BDTOutput_0j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_1j_B0=ww2l2v_13TeV_sr_1j_B0/BDTOutput_1j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_2j_B0=ww2l2v_13TeV_sr_2j_B0/BDTOutput_2j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_0j_B1=ww2l2v_13TeV_sr_0j_B1/BDTOutput_0j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_1j_B1=ww2l2v_13TeV_sr_1j_B1/BDTOutput_1j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_2j_B1=ww2l2v_13TeV_sr_2j_B1/BDTOutput_2j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_0j_B2=ww2l2v_13TeV_sr_0j_B2/BDTOutput_0j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_1j_B2=ww2l2v_13TeV_sr_1j_B2/BDTOutput_1j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_2j_B2=ww2l2v_13TeV_sr_2j_B2/BDTOutput_2j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_0j_B3=ww2l2v_13TeV_sr_0j_B3/BDTOutput_0j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_1j_B3=ww2l2v_13TeV_sr_1j_B3/BDTOutput_1j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_2j_B3=ww2l2v_13TeV_sr_2j_B3/BDTOutput_2j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_0j_B4=ww2l2v_13TeV_sr_0j_B4/BDTOutput_0j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_1j_B4=ww2l2v_13TeV_sr_1j_B4/BDTOutput_1j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_2j_B4=ww2l2v_13TeV_sr_2j_B4/BDTOutput_2j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_0j_B5=ww2l2v_13TeV_sr_0j_B5/BDTOutput_0j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_1j_B5=ww2l2v_13TeV_sr_1j_B5/BDTOutput_1j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_2j_B5=ww2l2v_13TeV_sr_2j_B5/BDTOutput_2j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_0j_B6=ww2l2v_13TeV_sr_0j_B6/BDTOutput_0j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_1j_B6=ww2l2v_13TeV_sr_1j_B6/BDTOutput_1j/datacard.txt  \
#                  ww2l2v_13TeV_ww_sr_eu_2j_B6=ww2l2v_13TeV_sr_2j_B6/BDTOutput_2j/datacard.txt  \
#                  ww2l2v_13TeV_top_0j=ww2l2v_13TeV_top_0j/events/datacard.txt \
#                  ww2l2v_13TeV_top_1j=ww2l2v_13TeV_top_1j/events/datacard.txt \
#                  ww2l2v_13TeV_top_2j=ww2l2v_13TeV_top_2j/events/datacard.txt \
#                  > datacard_combined.reg.txt

#OR
#cp datacard_combined.txt datacard_combined.reg.txt

#cat << EOT >> datacard_combined.reg.txt
#constr1 constr r_0-r_1 0.01
#constr2 constr r_1-r_2 0.01
#constr3 constr r_2-r_3 0.01
#constr4 constr r_3-r_4 0.01
#constr5 constr r_4-r_5 0.01
#constr6 constr r_5-r_6 0.01
#EOT

# Fix the imax, jmax and kmax lines in datacard txt file

text2workspace.py -P HiggsAnalysis.CombinedLimit.PhysicsModel:multiSignalModel \
                  --PO 'map=.*/.*WW_B0:r_0[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B1:r_1[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B2:r_2[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B3:r_3[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B4:r_4[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B5:r_5[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B6:r_6[1, -10, 10]' \
                  datacard_combined.reg.txt -o datacard_combined.reg.root

combine -M MultiDimFit --algo singles -t -1 --setParameters r_0=1,r_1=1,r_2=1,r_3=1,r_4=1,r_5=1,r_6=1 -d datacard_combined.reg.root --X-rtd MINIMIZER_analytic > datacard_combined.reg.out
