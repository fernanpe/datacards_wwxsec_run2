#!/bin/bash

## FIXME this is where the Combine framework is installed
#cd ~/Combine/CMSSW_10_2_13/src/
#eval `scramv1 runtime -sh`
#cd -

ulimit -s unlimited

combineCards.py WW2l2nu_2016_HIPM=../datacards_2016_HIPM/datacard_combined.txt \
                WW2l2nu_2016_noHIPM=../datacards_2016_noHIPM/datacard_combined.txt \
                WW2l2nu_2017=../datacards_2017/datacard_combined.txt \
                WW2l2nu_2018=../datacards_2018/datacard_combined.txt \
                > datacard_combined.txt

text2workspace.py -P HiggsAnalysis.CombinedLimit.PhysicsModel:multiSignalModel \
                  --PO 'map=.*/.*WW_B0:r_0[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B1:r_1[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B2:r_2[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B3:r_3[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B4:r_4[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B5:r_5[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B6:r_6[1, -10, 10]' \
        		  --PO 'map=.*/.*WW_B7:r_7[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B8:r_8[1, -10, 10]' \
                  --PO 'map=.*/.*WW_B9:r_9[1, -10, 10]' \
                  datacard_combined.txt -o datacard_combined.root

combine -M MultiDimFit --algo singles -t -1 --setParameters r_0=1,r_1=1,r_2=1,r_3=1,r_4=1,r_5=1,r_6=1,r_7=1,r_8=1,r_9=1 -d datacard_combined.root --X-rtd MINIMIZER_analytic > datacard_combined.out
