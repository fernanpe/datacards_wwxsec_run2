# Log update for review

[] a README file with instructions on how to run the combination of all the cards that are included. Please use this README as a reference. Please include instructions to run all the fits that are relevant for the analysis. You can put blinded results for the moment, and we will update them with the unblinded ones when you have them. Assignment falls on the authors.

README now included.

## Naming conventions

Change of the naming done as follows:

- `CMS_scale_JES*` -> `CMS_scale_j_*`
- `CMS_PU_2018` -> `CMS_pileup_2018`, `CMS_PU_2017` -> `CMS_pileup_2017`, `CMS_PU_2016` -> `CMS_pileup_2016`
- `CMS_PUID_2018` -> `CMS_eff_j_PUJET_id_2018`, `CMS_PUID_2017` -> `CMS_eff_j_PUJET_id_2017`, `CMS_PUID_2016` -> `CMS_eff_j_PUJET_id_2016` 
- `PS_ISR` -> `ps_isr`
- `PS_FSR` -> `ps_fsr`
- `UE_CP5` -> `underlying_event`

A few anomalies to be mentioned: 
- `CMS_fake_syst_em`, `CMS_fake_syst_me`, `CMS_fake_stat_m_2017`
- `pdf_Higgs`, `pdf_Higgs_ACCEPT`, `QCDscale_Higgs`, `QCDscale_Higgs_ACCEPT`

