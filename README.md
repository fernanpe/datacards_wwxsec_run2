# Introduction
This resource contains the full statistical details of the analysis [Measurement of inclusive and differential cross sections for
$W^+W^-$ production in proton-proton collisions at $\sqrt{s}$ = 13 TeV using FullRun2 data](https://cms.cern.ch/iCMS/analysisadmin/cadilines?line=SMP-24-008) by the CMS collaboration for review.
The statistical model is represented in the format of `Combine` datacards.
The instructions below include a few basic examples on how to extract the significance and signal strength measurements, for more details please consult the [`Combine` documentation](https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/).

# Datacards
While the full details of the selection can be found in the [Analysis Note](https://cms.cern.ch/iCMS/jsp/db_notes/noteInfo.jsp?cmsnoteid=CMS%20AN-2022/023), a short description of the datacards is contained in this repository follows.

The datacards in this repository concern mainly two types of cross section measurements:

* **Inclusive cross section**: which is a combination of the same-flavor (SF, i.e. $`ee`$ or $`\mu\mu`$) and different flavor (DF, i.e. $`e\mu`$) final states
* **Differential cross section**: which only uses the DF final state to have better signal efficiency

Starting from the highest order directory, as the name suggests:

* `inclusive_DF` directory coressponds to the datacards for inclusve Xsec measurement using DF final state
* `inclusive_SF` directory coressponds to the datacards for inclusve Xsec measurement using SF final state
* `inclusive_SF_DF` directory coressponds to the combined datacards for inclusve Xsec measurement using SF+DF final state from above
* `differential_DF` directory coressponds to the datacards for variables for which differential measurements will be performed using DF final state

The directories comrpise of datacards for all the Run2 years separately (2016-2018), as well as combined FullRun2 datacards. 

   * `datacards_2016_HIPM`
   * `datacards_2016_noHIPM`
   * `datacards_2017`
   * `datacards_2018`   

`datacard_combined.txt{.root}`) is the representative datacard txt (root) file in each subdirectory. 

The nuisance parameters corresponding to different sources of systematic uncertainties are described in the `*.html` files located in the `input` directory.

# Software instructions

General installation instructions for `Combine` can be found in the [`Combine` documentation](https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/).


# Inclusive cross section measurement

To compute the inclusive cross section the combination involved the following steps:

1. The combination of the cards corresponding to the different categories:
```
   combineCards.py   ww2l2v_13TeV_ww_sr_eu_0j_SF_2016HIPM=${datacardDir_2016_HIPM_SF}/ww2l2v_13TeV_sr_0j_B0/events/datacard.txt  \
      ww2l2v_13TeV_ww_sr_eu_1j_SF_2016HIPM=${datacardDir_2016_HIPM_SF}/ww2l2v_13TeV_sr_1j_B0/events/datacard.txt  \
      ww2l2v_13TeV_ww_sr_eu_2j_SF_2016HIPM=${datacardDir_2016_HIPM_SF}/ww2l2v_13TeV_sr_2j_B0/events/datacard.txt  \
      ww2l2v_13TeV_top_0j_SF_2016HIPM=${datacardDir_2016_HIPM_SF}/ww2l2v_13TeV_top_0j/events/datacard.txt \
      ww2l2v_13TeV_top_1j_SF_2016HIPM=${datacardDir_2016_HIPM_SF}/ww2l2v_13TeV_top_1j/events/datacard.txt \
      ww2l2v_13TeV_top_2j_SF_2016HIPM=${datacardDir_2016_HIPM_SF}/ww2l2v_13TeV_top_2j/events/datacard.txt \
      > Full2016_HIPM_incl_SF.txt

   combineCards.py   ww2l2v_13TeV_ww_sr_eu_0j_SF_2016noHIPM=${datacardDir_2016_noHIPM_SF}/ww2l2v_13TeV_sr_0j_B0/events/datacard.txt  \
      ww2l2v_13TeV_ww_sr_eu_1j_SF_2016noHIPM=${datacardDir_2016_noHIPM_SF}/ww2l2v_13TeV_sr_1j_B0/events/datacard.txt  \
      ww2l2v_13TeV_ww_sr_eu_2j_SF_2016noHIPM=${datacardDir_2016_noHIPM_SF}/ww2l2v_13TeV_sr_2j_B0/events/datacard.txt  \
      ww2l2v_13TeV_top_0j_SF_2016noHIPM=${datacardDir_2016_noHIPM_SF}/ww2l2v_13TeV_top_0j/events/datacard.txt \
      ww2l2v_13TeV_top_1j_SF_2016noHIPM=${datacardDir_2016_noHIPM_SF}/ww2l2v_13TeV_top_1j/events/datacard.txt \
      ww2l2v_13TeV_top_2j_SF_2016noHIPM=${datacardDir_2016_noHIPM_SF}/ww2l2v_13TeV_top_2j/events/datacard.txt \
      > Full2016_noHIPM_incl_SF.txt

   combineCards.py   ww2l2v_13TeV_ww_sr_eu_0j_SF_2017=${datacardDir_2017_SF}/ww2l2v_13TeV_sr_0j_B0/events/datacard.txt  \
      ww2l2v_13TeV_ww_sr_eu_1j_SF_2017=${datacardDir_2017_SF}/ww2l2v_13TeV_sr_1j_B0/events/datacard.txt  \
      ww2l2v_13TeV_ww_sr_eu_2j_SF_2017=${datacardDir_2017_SF}/ww2l2v_13TeV_sr_2j_B0/events/datacard.txt  \
      ww2l2v_13TeV_top_0j_SF_2017=${datacardDir_2017_SF}/ww2l2v_13TeV_top_0j/events/datacard.txt \
      ww2l2v_13TeV_top_1j_SF_2017=${datacardDir_2017_SF}/ww2l2v_13TeV_top_1j/events/datacard.txt \
      ww2l2v_13TeV_top_2j_SF_2017=${datacardDir_2017_SF}/ww2l2v_13TeV_top_2j/events/datacard.txt \
      > Full2017_incl_SF.txt

   combineCards.py   ww2l2v_13TeV_ww_sr_eu_0j_SF_2018=${datacardDir_2018_SF}/ww2l2v_13TeV_sr_0j_B0/events/datacard.txt  \
      ww2l2v_13TeV_ww_sr_eu_1j_SF_2018=${datacardDir_2018_SF}/ww2l2v_13TeV_sr_1j_B0/events/datacard.txt  \
      ww2l2v_13TeV_ww_sr_eu_2j_SF_2018=${datacardDir_2018_SF}/ww2l2v_13TeV_sr_2j_B0/events/datacard.txt  \
      ww2l2v_13TeV_top_0j_SF_2018=${datacardDir_2018_SF}/ww2l2v_13TeV_top_0j/events/datacard.txt \
      ww2l2v_13TeV_top_1j_SF_2018=${datacardDir_2018_SF}/ww2l2v_13TeV_top_1j/events/datacard.txt \
      ww2l2v_13TeV_top_2j_SF_2018=${datacardDir_2018_SF}/ww2l2v_13TeV_top_2j/events/datacard.txt \
      > Full2018_incl_SF.txt

   combineCards.py   ww2l2v_13TeV_ww_sr_eu_0j_DF_2016HIPM=${datacardDir_2016_HIPM_DF}/ww2l2v_13TeV_sr_0j_B0/BDTOutput_0j/datacard.txt  \
      ww2l2v_13TeV_ww_sr_eu_1j_DF_2016HIPM=${datacardDir_2016_HIPM_DF}/ww2l2v_13TeV_sr_1j_B0/BDTOutput_1j/datacard.txt  \
      ww2l2v_13TeV_ww_sr_eu_2j_DF_2016HIPM=${datacardDir_2016_HIPM_DF}/ww2l2v_13TeV_sr_2j_B0/BDTOutput_2j/datacard.txt  \
      ww2l2v_13TeV_top_0j_DF_2016HIPM=${datacardDir_2016_HIPM_DF}/ww2l2v_13TeV_top_0j/events/datacard.txt \
      ww2l2v_13TeV_top_1j_DF_2016HIPM=${datacardDir_2016_HIPM_DF}/ww2l2v_13TeV_top_1j/events/datacard.txt \
      ww2l2v_13TeV_top_2j_DF_2016HIPM=${datacardDir_2016_HIPM_DF}/ww2l2v_13TeV_top_2j/events/datacard.txt \
      > Full2016_HIPM_incl_DF.txt

   combineCards.py   ww2l2v_13TeV_ww_sr_eu_0j_DF_2016noHIPM=${datacardDir_2016_noHIPM_DF}/ww2l2v_13TeV_sr_0j_B0/BDTOutput_0j/datacard.txt  \
      ww2l2v_13TeV_ww_sr_eu_1j_DF_2016noHIPM=${datacardDir_2016_noHIPM_DF}/ww2l2v_13TeV_sr_1j_B0/BDTOutput_1j/datacard.txt  \
      ww2l2v_13TeV_ww_sr_eu_2j_DF_2016noHIPM=${datacardDir_2016_noHIPM_DF}/ww2l2v_13TeV_sr_2j_B0/BDTOutput_2j/datacard.txt  \
      ww2l2v_13TeV_top_0j_DF_2016noHIPM=${datacardDir_2016_noHIPM_DF}/ww2l2v_13TeV_top_0j/events/datacard.txt \
      ww2l2v_13TeV_top_1j_DF_2016noHIPM=${datacardDir_2016_noHIPM_DF}/ww2l2v_13TeV_top_1j/events/datacard.txt \
      ww2l2v_13TeV_top_2j_DF_2016noHIPM=${datacardDir_2016_noHIPM_DF}/ww2l2v_13TeV_top_2j/events/datacard.txt \
      > Full2016_noHIPM_incl_DF.txt

   combineCards.py   ww2l2v_13TeV_ww_sr_eu_0j_DF_2017=${datacardDir_2017_DF}/ww2l2v_13TeV_sr_0j_B0/BDTOutput_0j/datacard.txt  \
      ww2l2v_13TeV_ww_sr_eu_1j_DF_2017=${datacardDir_2017_DF}/ww2l2v_13TeV_sr_1j_B0/BDTOutput_1j/datacard.txt  \
      ww2l2v_13TeV_ww_sr_eu_2j_DF_2017=${datacardDir_2017_DF}/ww2l2v_13TeV_sr_2j_B0/BDTOutput_2j/datacard.txt  \
      ww2l2v_13TeV_top_0j_DF_2017=${datacardDir_2017_DF}/ww2l2v_13TeV_top_0j/events/datacard.txt \
      ww2l2v_13TeV_top_1j_DF_2017=${datacardDir_2017_DF}/ww2l2v_13TeV_top_1j/events/datacard.txt \
      ww2l2v_13TeV_top_2j_DF_2017=${datacardDir_2017_DF}/ww2l2v_13TeV_top_2j/events/datacard.txt \
      > Full2017_incl_DF.txt

   combineCards.py   ww2l2v_13TeV_ww_sr_eu_0j_DF_2018=${datacardDir_2018_DF}/ww2l2v_13TeV_sr_0j_B0/BDTOutput_0j/datacard.txt  \
      ww2l2v_13TeV_ww_sr_eu_1j_DF_2018=${datacardDir_2018_DF}/ww2l2v_13TeV_sr_1j_B0/BDTOutput_1j/datacard.txt  \
      ww2l2v_13TeV_ww_sr_eu_2j_DF_2018=${datacardDir_2018_DF}/ww2l2v_13TeV_sr_2j_B0/BDTOutput_2j/datacard.txt  \
      ww2l2v_13TeV_top_0j_DF_2018=${datacardDir_2018_DF}/ww2l2v_13TeV_top_0j/events/datacard.txt \
      ww2l2v_13TeV_top_1j_DF_2018=${datacardDir_2018_DF}/ww2l2v_13TeV_top_1j/events/datacard.txt \
      ww2l2v_13TeV_top_2j_DF_2018=${datacardDir_2018_DF}/ww2l2v_13TeV_top_2j/events/datacard.txt \
      > Full2018_incl_DF.txt

   combineCards.py WW2l2nu_2016_HIPM_SF=Full2016_HIPM_incl_SF.txt \
      WW2l2nu_2016_noHIPM_SF=Full2016_noHIPM_incl_SF.txt \
      WW2l2nu_2017_SF=Full2017_incl_SF.txt \
      WW2l2nu_2018_SF=Full2018_incl_SF.txt \
      WW2l2nu_2016_HIPM_DF=Full2016_HIPM_incl_DF.txt \
      WW2l2nu_2016_noHIPM_DF=Full2016_noHIPM_incl_DF.txt \
      WW2l2nu_2017_DF=Full2017_incl_DF.txt \
      WW2l2nu_2018_DF=Full2018_incl_DF.txt \
      > FullRunII_incl.txt                                
   ```

2. The creation of the workspace with the default parameter of interest `r` scaling all the signal processes together (including the non-fiducial):
   ```
   text2workspace.py FullRunII_incl.txt -o FullRunII_incl.root -m 125
   ```

3. Running the fit:
   ```
   combine -M MultiDimFit --algo singles --rMin=-10 --rMax=10 FullRunII_incl.txt -t -1 --expectSignal=1 -m 125 
   ```

This will result in an output similar to the following:
```
 <<< Combine >>>
>>> method used is MultiDimFit
>>> random number generator seed is 123456
.....
.....
 --- MultiDimFit ---
best fit parameter values and profile-likelihood uncertainties:
   r :    +1.000   -0.033/+0.037 (68%)
```

The fitted value of `r` multiplied by the expected cross section (<X> pb) gives the inclusive cross section measured.

# Fiducial and differential cross section measurement

To get from the parameters of interest of the fit (cros section multipliers) to cross sections, the following table of expected ccros sections should be used:

The following steps are needed:

1. The combination of the cards corresponding to the different categories:
   
   ```
   combineCards.py   ww2l2v_13TeV_sr_2j_B0=${datacardDir_2018}/ww2l2v_13TeV_sr_2j_B0/BDTOutput_2j/datacard.txt  \
                     ww2l2v_13TeV_sr_2j_B1=${datacardDir_2018}/ww2l2v_13TeV_sr_2j_B1/BDTOutput_2j/datacard.txt  \
                     ww2l2v_13TeV_sr_2j_B2=${datacardDir_2018}/ww2l2v_13TeV_sr_2j_B2/BDTOutput_2j/datacard.txt  \
                     ww2l2v_13TeV_sr_2j_B3=${datacardDir_2018}/ww2l2v_13TeV_sr_2j_B3/BDTOutput_2j/datacard.txt  \
                     ww2l2v_13TeV_sr_2j_B4=${datacardDir_2018}/ww2l2v_13TeV_sr_2j_B4/BDTOutput_2j/datacard.txt  \
                     ww2l2v_13TeV_sr_2j_B5=${datacardDir_2018}/ww2l2v_13TeV_sr_2j_B5/BDTOutput_2j/datacard.txt  \
                     ww2l2v_13TeV_sr_2j_B6=${datacardDir_2018}/ww2l2v_13TeV_sr_2j_B6/BDTOutput_2j/datacard.txt  \
                     ww2l2v_13TeV_sr_2j_B7=${datacardDir_2018}/ww2l2v_13TeV_sr_2j_B7/BDTOutput_2j/datacard.txt  \
                     ww2l2v_13TeV_sr_2j_B8=${datacardDir_2018}/ww2l2v_13TeV_sr_2j_B8/BDTOutput_2j/datacard.txt  \
                     ww2l2v_13TeV_sr_2j_B9=${datacardDir_2018}/ww2l2v_13TeV_sr_2j_B9/BDTOutput_2j/datacard.txt  \
                     ww2l2v_13TeV_sr_2j_B10=${datacardDir_2018}/ww2l2v_13TeV_sr_2j_B10/BDTOutput_2j/datacard.txt  \
                     ww2l2v_13TeV_sr_2j_B11=${datacardDir_2018}/ww2l2v_13TeV_sr_2j_B11/BDTOutput_2j/datacard.txt  \
                     ww2l2v_13TeV_sr_2j_B12=${datacardDir_2018}/ww2l2v_13TeV_sr_2j_B12/BDTOutput_2j/datacard.txt  \
                     ww2l2v_13TeV_sr_2j_B13=${datacardDir_2018}/ww2l2v_13TeV_sr_2j_B13/BDTOutput_2j/datacard.txt  \
                     ww2l2v_13TeV_top_2j=${datacardDir_2018}/ww2l2v_13TeV_top_2j/events/datacard.txt \
                     > Full2018_dphijj_bdt.txt
   ```

   ```
   combineCards.py WW2l2nu_2016_HIPM=../datacards_2016_HIPM/datacard_combined.txt \
                WW2l2nu_2016_noHIPM=../datacards_2016_noHIPM/datacard_combined.txt \
                WW2l2nu_2017=../datacards_2017/datacard_combined.txt \
                WW2l2nu_2018=../datacards_2018/datacard_combined.txt \
                > datacard_combined.txt
   ```

2. The workspace is created using the `Combine` internal physics model allowing to scale the signal in the different generator level jet bins with different multipliers:
   ```
   text2workspace.py -P HiggsAnalysis.CombinedLimit.PhysicsModel:multiSignalModel \
                     --PO 'map=.*/.*WW_B0:r_0[1, -10, 10]' \
                     --PO 'map=.*/.*WW_B1:r_1[1, -10, 10]' \
                     --PO 'map=.*/.*WW_B2:r_2[1, -10, 10]' \
                     --PO 'map=.*/.*WW_B3:r_3[1, -10, 10]' \
                     --PO 'map=.*/.*WW_B4:r_4[1, -10, 10]' \
                     --PO 'map=.*/.*WW_B5:r_5[1, -10, 10]' \
                     --PO 'map=.*/.*WW_B6:r_6[1, -10, 10]' \
                     --PO 'map=.*/.*WW_B7:r_7[1, -10, 10]' \
                     --PO 'map=.*/.*WW_B8:r_8[1, -10, 10]' \
                     --PO 'map=.*/.*WW_B9:r_9[1, -10, 10]' \
                     --PO 'map=.*/.*WW_B10:r_10[1, -10, 10]' \
                     --PO 'map=.*/.*WW_B11:r_11[1, -10, 10]' \
                     --PO 'map=.*/.*WW_B12:r_12[1, -10, 10]' \
                     --PO 'map=.*/.*WW_B13:r_13[1, -10, 10]' \
                     Full2018_dphijj_bdt.txt -o Full2018_dphijj_bdt.root
   ```

3. The fit is run:
   ```
   combine -M MultiDimFit --algo singles workspace_opt1.rootcombine -M MultiDimFit --algo singles -t -1 --setParameters r_0=1,r_1=1,r_2=1,r_3=1,r_4=1,r_5=1,r_6=1,r_7=1,r_8=1,r_9=1,r_10=1,r_11=1,r_12=1,r_13=1 -d datacard_combined.root --X-rtd MINIMIZER_analytic > datacard_combined.out
   ```

The resulting output will be:
```
XYZ
```

The fitted values of the cross section multipliers, multiplied by the expected cross section in the corresponding jet multiplicity bins given in the table above, will yield the measured differential cross section.



The fiducial cross section is given by the value of `r` multiplied by the fiducial expected cross section in the table above.
